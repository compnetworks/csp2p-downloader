# Part B: Peer-to-peer protocol #

In the peer-to-peer approach, the client must first obtain the *torrent metadata* from the torrent server (a.k.a *tracker*) and use this metadata to download data blocks. The torrent metadata contains information about the number and size of blocks constituting the file and peers (`<IP,port>` tuples) from which the blocks may be downloaded.


## Torrent metadata ##

**Torrent metadata request format**: The client must download the torrent metadata for filename by sending a UDP message in the following format:

```
	GET <filename>.torrent\n
```
Thus, to request the torrent metadata for `redsox.jpg`, the client must send a UDP message containing the string `GET redsox.jpg.torrent` (with or without a newline at the end) to the torrent server.

This UDP-based torrent server is running at `<date.cs.umass.edu, 19876>`. Just like in Part A, we strongly encourage you to run your own server on your local machine.

**Torrent metadata response format**: The response to the request for torrent metadata is in the following format:

```
	NUM_BLOCKS: 6
	FILE_SIZE: 58241
	IP1: 128.119.245.20
	PORT1: 3456
	IP2: 128.119.245.20
	PORT2: 4321
```

The names of the fields above are self-explanatory. `NUM_BLOCKS` is the number of blocks in the requested file. `FILE_SIZE` is the size of the entire file in bytes. `IP1` and `PORT1` identify the IP address and port number of the first peer, and `IP2` and `PORT2` the second peer.

Each response will contain two randomly chosen valid peer identifiers. You can query the tracker multiple times to get more peer identifiers. However, the tracker is designed to rate-limit the queries, so you may not get responses promptly if you send requests too fast or may not get responses at all as UDP messages can get lost.

## Data blocks ##

Having obtained metadata information using UDP as above, data blocks must be requested using TCP as follows.

**Block request format**: The following request fetches a specific block

```
	GET filename:<block_number>\n
```

where block_number is an integer identifying the block in filename. For example, you may request block 24 in redsox.jpg by sending the string `GET Redsox.jpg:24\n` to any one of the peers received in the torrent metadata above. Note that the servers listed in the client/server option also act as peers and support the above request format to request specific blocks.

Specifying `*’ instead of a block number returns a randomly chosen block

```
	GET filename:*\n
```

**Block response format**: The response to a block request has the following format as that of the whole body. The only difference is that the starting byte offset in the file in general will be non-zero and the size of the block will be much smaller than the size of the file, for example:

```
	200 OK
	BODY_BYTE_OFFSET_IN_FILE: 20000
	BODY_BYTE_LENGTH: 10000

	^#@gdhh#...<bytes of body follow here>
```

All blocks except the last block will be of the same size.

# Part B Deliverable #
Your client must implement the torrent metadata protocol over a UDP socket to the torrent server's `<IP,port>` specified as a command-line argument, learn `<IP,port>` peer tuples from the torrent server, retrieve all the blocks of the `filename` specified as a command-line argument (refer Submission Instructions), store the retrieved body in a local file of the same name in the current directory, and then exit gracefully by closing all sockets.

Speed is of essence in the peer-to-peer approach, so your client must implement a strategy to download the file as fast as possible. The server ports on the peer-to-peer server are intentionally rate-limited to send data slower compared to the client-server server, so you will need to use the peer-to-peer approach to download the file in a reasonable amount of time.

