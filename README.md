[TOC]

# Overview #

This two-part assignment is designed to introduce you to socket programming by making you implement a file downloader using a traditional client/server approach as well as a peer-to-peer approach.

**Goal**: Your goal is to write a network program, referred to as the *client*, that downloads an image file from a *server* that we maintain, and your client must implement support for the following two options:
	
1. Client/server option: Request the server for the entire file (similar in spirit to HTTP).
	
2. Peer-to-peer option: Request the server for addresses of other peers that possess parts of the file, called *blocks*, and download these blocks from different peers (similar in spririt to BitTorrent).

The following sections include a Getting Started tutorial; the protocol and deliverable for the client/server option; the protocol and deliverable for the peer-to-peer option; submission and auto-grading instructions; and tips and FAQs.

# Getting Started #

Refer to and follow the steps in this [Getting Started with TCP](https://bitbucket.org/compnetworks/sockets/src/master/README.md) tutorial. Make sure you have run every (corresponding) pair of examples provided and understand why they behave the way they do. Without this step, you will find programming with TCP frustratingly difficult. After completing that tutorial, move on to Part A below.

# Part A: Client-Server Protocol and Deliverable#

Refer [here](https://bitbucket.org/compnetworks/csp2p-downloader/src/master/PartA.md).

# Part B: Peer-to-Peer Protocol and Deliverable#

Refer [here](https://bitbucket.org/compnetworks/csp2p-downloader/src/master/PartB.md).

# Submission Instructions #

1. You must submit your client program. For part A, name the main file `CSDownloader.<appropriate-extension>`, e.g., `CSDownloader.py` for python. For part B, name the main file `P2PDownloader.<appropriate-extension>`. Feel free to code up additional helper files to this main file as needed. 

2. You can use any programming language of your choice. The nice part about network programming using the socket API is that the server and client may be written in different programming languages.

3. Your client must implement support for the following command-line argument format. For example, if you are using java or python or C, we should be able to invoke your client as follows respectively for parts A and B:
```
        java CSDownloader <filename> <clientServerIP> <clientServerPort>
        java P2PDownloader <filename> <torrentServerIP> <torrentServerPort>
		
		python CSDownloader.py <filename> <clientServerIP> <clientServerPort>
        python P2PDownloader.py <filename> <torrentServerIP> <torrentServerPort>
		
		CSDownloader.o <filename> <clientServerIP> <clientServerPort>
        P2PDownloader.o <filename> <torrentServerIP> <torrentServerPort>
```
	Note that the IP and port are those of the TCP-based file server in part A and the UDP-based torrent metadata server in part B. *Do not hardcode any filenames or IP/port tuples in your code as your code may be tested with different files and servers than the ones provided for development*.

4. For both parts A and B, your client must dowload the specified file into a file of the same name in the current directory.

5. Submit all your code files with your appropriately named client program in the top-level directory. The autograde will simply look for the file `./CSDownloader.py` or `./P2PDownloader.py`, so don't impose any sub-directory structure for any code files.

# Tips, FAQs, etc. #

Refer [here](Tips.md). The overwhelimg majority of common questions or roadblocks are likely to be resolved by taking the time to carefully read and follow these tips.