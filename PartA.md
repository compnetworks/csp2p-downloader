[TOC]

# Part A: Client/server protocol #

In the client/server approach, the client should request the server for the entire file using TCP. We maintain a server running on the following <hostname, port number> combination: `<pear.cs.umass.edu, 18765>` for convenience of your client development and testing. However, you don't need to rely on our server as the [source code of the server](https://bitbucket.org/compnetworks/csp2p/src/master/) is public, so you can clone and run the server yourself -- the recommended option -- on your local machine during development and testing.

## File request ##
The client must send a request in the following format to the server to download filename, where the `\n` at the end is the newline character, and `<filename>` should be replaced with the name of the file to be downloaded (without the `<` and `>` enclosers).


```
	GET <filename>\n
```

## File response ##
The server's response to a correctly formatted file request will have a header followed by the body. The *header* is terminated by two newline characters, i.e., the string `\n\n`, which also marks the beginning of the *body*. An example response is as follows:

```
	200 OK
	BODY_BYTE_OFFSET_IN_FILE: 0
	BODY_BYTE_LENGTH: 58241

	b^*%_JE@(u...<bytes of body follow here>
```

The example response above contains four lines of header interpreted as follows: The first line consists of a numeric status code (`200` above), whitespace, and a message (`OK` above) terminated by a newline character. The next line contains a header keyword `BODY_BYTE_OFFSET_IN_FILE` that specifies from what byte position in the file does the body (on the fifth line above) start. In the client/server mode, the server will always return the whole file in one piece starting from byte 0 (or the first byte), so you can simply ignore this line. The third line specifies the number of bytes in the body (similar to `Content-Length` in HTTP). The line following the empty line is where the bytes of the body start, so in the above example, the first byte (`b` above) on the fifth line is the first (offset 0) byte of the file and a total of 58241 bytes are being returned by the server in the body. 

The server will return `200 OK` for a correctly formatted request; for incorrectly formatted requests or requests for non-existent files, the server will return `400 BAD_FORMAT`. For testing and development, the server serves these two files: `test.jpg` and `redsox.jpg`.

# Part A Deliverable #
Your client must programmatically "speak" the above protocol over a TCP socket connected to the `<IP,port>` specified as a command-line argument, retrieve all the bytes of the body of the `filename` also specified as a command-line argument (refer Submission Instructions), store the retrieved body in a local file of the same name in the current directory, and then exit gracefully.
