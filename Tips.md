[TOC]

# Part A #

1. You are strongly encouraged to run and thoroughly understand these ["hello world" socket programming examples](https://bitbucket.org/compnetworks/sockets/src/master/). To be able to do part A, you need to be able to clearly articulate what a "bytestream" means as opposed to a "datagram".

2. Use `telnet` as shown in several class examples to "speak" TCP-based protocols as a quick manual (non-programmatic) check that they work as expected.

3. Do not assume you "know" how a socket method you have never used before ought to work based on seeing one hello world program. Read the documentation of whatever method you are using in your programming language in order to distinguish expected behavior from seemingly buggy behavior.

4. You can use our servers but are strongly encouraged to run the server yourself on your local machine by following the instructions [here](https://bitbucket.org/compnetworks/csp2p/src/master/) for development and testing before you submit your code to Gradescope's autograder.

5. Do not submit to Gradescope until you are done with development and testing as the Gradescope autograder server will be unnecessarily slow. Also see tip #15 below on false positives and negatives: Gradescope can only tell you what test it is failing, which may not always be a useful hint on what mistake you are making. That needs a human, and you are ultimately responsible for fixing bugs in your code.

6. The testing files `test.jpg` and `redsox.jpg` are [here]("https://bitbucket.org/compnetworks/csp2p/src/master/resources/") so that you can check (e.g., using unix `diff` or windows `fc` or similar) that the file you are writing is identical to the original files. 

7. Your code may be tested with other files or tests beyond the ones on the Gradescope autograder's public tests.

More tips or FAQs to be added here based on class questions.

# Part B #

8. **Early bird gets the worm**: Plan to complete the assignment well before the due date as successful execution of the entire program to download the file may take several minutes in Part B, so debugging and testing may take longer than you might expect, and you are more likely to be able to incorporate help or feedback from us if needed.

9. **Threads are your friends**: As you may have realized, common socket methods are *blocking*, so you are encouraged to use multiple threads to simultaneously download blocks from different peers to improve performance, but be careful in synchronizing concurrent access to shared data structures with threads (which if you have not taken an OS course or do not otherwise have experience using threads may incur a steeper learning curve.)

10. **Document, document, document**: Comment your code as much as possible keeping in mind that documentation is as much for your own benefit as for the benefit of others who read your code, especially if we happen to need to manually grade your submission.

11. **Anything that can go wrong will**: Your connections will get closed if you send bad commands, and at random times, your connection may also get closed for no good reason as the server periodically changes the peer-to-peer ports, so your code should be resilient to unexpected failures.

12. **Find good friends and keep 'em**: The total number of open connections your client can use (across all peer ports) will be limited to a small number based on your IP address, so be careful about remembering to close idle connections because if you try to open more connections than the limit, they will get immediately closed, and furthermore, the system will automatically close connections that have been idle for over a minute, so forgetting to close connections in the long past doesn't haunt you.

13. Reminders: 
	1. Reminder: Part B’s server maintained by us is different from Part A’s.

	2. Reminder: TCP != UDP. All data transfer uses TCP, but the torrent metadata tracker uses UDP, so you can not make a TCP connection to or telnet to a UDP server, however you can use `UDPTelnet.java` from the provided “hello world” examples or a command-line tool like `nc` to manually (non-programmatically) “speak” the torrent server protocol on a console just for checking that it works as described.
	
	3. Reminder: You are strongly encouraged to explore and use any convenient methods supported by the Java/Python/your-favorite-language's socket API, but make sure to remember tip #3 from part A.
	
	4. Reminder: Blocks are numbered starting from 0, and block offsets (the position of the first byte of the block in the file) start from 0.
	
	5. Reminder: The UDP torrent metadata server is rate-limited, so not every request may receive a response (which goes without saying with UDP in general anyway).
	
	6. Reminder: The data block peer servers are rate-limited, so data may trickle in at its own sweet pace (which goes without saying for a TCP bytestream in general anyway). 
	
14. You have another utility command called `GETHDR` available to just get the header, which sends only the header, not the body, and has been provided to quickly check that the server ports are up and running as expected. (Your client does not need to use this command.)

15. Gradescope autograder:
	1. The autograder's public tests are provided to help nudge you in the right direction, but they are *NOT* meant to be 
		* *complete* (in that it is not meant to detect all of your bugs) 
		* or *sound* (in that it may assign you a very low score because, say, because it couldn't find your downloaded file at the expected place even though most of the rest of your code may be correct) 
		* or *accurate* (in that your final score may differ from what you see last because we may re-run your code multiple times or because the weighting of speed-related tests may be changed somewhat based on overall class performance).
		
		All autograder scores will be manually reviewed before we finalize grades.
	
	2. A Makefile is supported for languages like C/C++ or others that are not explicitly supported by the compile script.


More tips or FAQs to be added here based on class questions.

